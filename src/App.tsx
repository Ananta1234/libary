import React from 'react';
import {Form,Button,TextField} from './form-libary'
function App() {
  const handleFirst = (obj: any) =>{
    console.log(obj,'11111')
  }
  const handleSecond = (obj: any) =>{
    console.log(obj,'22222')
  }

  return (
    <div className="App">
        <Form onFinish= {handleFirst} name="test1">
            <TextField placeholder="username" name="username" />
            <Button>Submit</Button>
        </Form>
        <br></br>
        <Form  onFinish= {handleSecond} name="test2">
            <TextField placeholder="username" name="username" />
            <Button>Submit</Button>
        </Form>
        
    </div>
  );
}

export default App;
