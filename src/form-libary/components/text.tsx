import React from "react";
import {registerData} from '../logic'
export interface ITextProps{
    inputWrapperClassName?:string,
    type?:string,
    name:string,
    className?:string,
    defaultValue?:string | number
}


export const TextField : React.FC<ITextProps &
React.InputHTMLAttributes<HTMLInputElement>> = ({
    inputWrapperClassName,
    type,
    name,
    ...props
}) =>{
    const {register} = registerData()
    return(
        <div className={`form-input-wrapper ${inputWrapperClassName ?? ''}`}>
            <input type = {type ?? 'text'} {...props} name="name" onChange={register} />
        </div>
    )
}