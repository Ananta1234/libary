import React from "react"
export interface IButtonProps{
  title?:string,
  buttonWrapperClassName?:string
}

export  const Button: React.FC<IButtonProps & 
React.ButtonHTMLAttributes<HTMLButtonElement>> = ({
    children,
    title,
    buttonWrapperClassName,
    ...props
}) =>{
    return(
      <div className={`form-button-wrapper ${buttonWrapperClassName ?? ''}`}>
            <button {...props}>
                {title ?? children}
            </button>
      </div>
    )
}