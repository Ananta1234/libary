import React from "react"

import {useForm} from '../hooks'
export interface IFormProps{
    onFinish?:CallableFunction,
    formWrapperClassName?:string,
    className?:string,
    name:string
}

export const Form: React.FC<IFormProps & 
React.FormHTMLAttributes<HTMLFormElement>> = ({
    children,
    onFinish,
    formWrapperClassName,
    ...props
}) =>{
    const {handleSubmit} = useForm(onFinish)

    return(
       <div className={`form-wrapper ${formWrapperClassName ?? ''}`}>
            <form {...props} onSubmit ={handleSubmit} >
                {children}
            </form>
       </div>
    )
}