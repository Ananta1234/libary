import React from "react"
import {registerData} from '../logic'
export function useForm(callback :any,initialValues={}){
    const {formData} = registerData()
   
    const handleSubmit = (event: React.SyntheticEvent) =>{
        event.preventDefault()
        console.log(formData)
        callback()
    }


    return{
        handleSubmit,
    }
}